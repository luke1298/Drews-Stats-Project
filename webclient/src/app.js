/* eslint react/prop-types: 0 */

import React from 'react';
import ReactDOM from 'react-dom';
import Select from 'react-select';

import Pitchers from './components/Pitchers';
import Batters from './components/Batters';
import d3 from 'd3';

var http = require("http");

var Player = React.createClass({
  render: function() {
    return (
      <ul>
        {
          this.props.player.map(function(player) {
            return <li className={player.color}>Name: {player.name}  <ul> <li> Pitch Speed: {player.plate_speed} </li> <li> Probability of Swinging Strike: {player.prob}  </li> <li> Pitch Type: {player.pitch_type}  </li> </ul></li>;
          })
        }
      </ul>
    );
  }
});
function getPlayers(){
    var players_array = [];
    var parent = document.getElementsByClassName("pitchers-list")[0]
    var player_tags = parent.getElementsByClassName("Select-value-label");
    for (var tag=0; tag < player_tags.length; tag++){
        players_array.push(player_tags[tag].textContent);
    }
    return players_array;
}
var make_plots = function() {
    var batter = Batters.id()
    var pitchers = Pitchers.ids()
    console.log(batter);
    console.log(pitchers);
    if (!batter || !pitchers){
        throw("Invalid Input");
    }
    var url = "http://localhost:8080/api/exampleR/"
    url = url + batter + "/";
    url = url + pitchers.replace(new RegExp(',', 'g'), ' ');
    http.get(url, function(res){
        res.setEncoding('utf8')
        res.on('data', function(incoming_data){
            console.log(url);
            var data = [];
            var cleaned_data = JSON.parse(incoming_data);
            var item;
            console.log(cleaned_data);
            for (item in cleaned_data){
                data.push(cleaned_data[item][0]);
            }
            draw(data);
        })
    });

    function draw(data){
        var players_array = getPlayers();
        for (x in data){
            console.log(data[x])
            data[x].name = players_array[x]
            console.log(data[x])
        }

        data.sort(function(a, b){
            return -1*(a.prob-b.prob)
        })

        for (x in data){
            console.log(x);
            console.log(data[x]);
            data[x].color = rank_probabilities(x);
        }

        function rank_probabilities(i){
            if (i == 0){
                return "red";
            }
            else if (i==1){
                return "green";
            }
            else if (i==2){
                return "blue";
            }
            else if (i==3){
                return "orange";
            }
            else if (i==4){
                return "silver";
            }
            else if (i==5){
                return "purple";
            }
        }

        console.log(data)
        var margin = {top: 60, right: 60, bottom: 60, left: 60}
          , width = 700 - margin.left - margin.right
          , height = 800 - margin.top - margin.bottom;

        var x = d3.scale.linear()
                  .domain([-17.0/24, 17.0/24])
                  .range([ 0, width ]);

        var y = d3.scale.linear()
                  .domain([1.2, 3.5])
                  .range([ height, 0 ]);

        d3.select('#strike-zone').select("svg").remove();
        var chart = d3.select('#strike-zone')
        .append('svg:svg')
        .attr('width', width + margin.right + margin.left)
        .attr('height', height + margin.top + margin.bottom)
        .attr('class', 'chart')

        var main = chart.append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'main')

        // draw the x axis
        var xAxis = d3.svg.axis()
        .scale(x)
        .orient('bottom');

        main.append('g')
        .attr('transform', 'translate(0,' + height + ')')
        .attr('class', 'main axis date');

        // draw the y axis
        var yAxis = d3.svg.axis()
        .scale(y)
        .orient('left');

        main.append('g')
        .attr('transform', 'translate(0,0)')
        .attr('class', 'main axis date');

        var g = main.append("svg:g");

        g.selectAll("line-x")
          .data([0,1,2,3])
          .enter().append("line")
          .attr("x1", function(d) { return x((d*(17.0/36))-17.0/24);})
          .attr("x2", function(d) { return x((d*(17.0/36))-17.0/24);})
          .attr("y1", 0)
          .attr("y2", 591.304347826087)
          .style("stroke", "grey");

        g.selectAll(".rule-x")
          .data([0,1,2,3])
          .enter().append("text")
          .attr("class", "rule")
          .attr("x", function(d) { return x((d*(17.0/36))-17.0/24); })
          .attr("y", 591.304347826087+20)
          .attr("dy", -6)
          .attr("text-anchor", "middle")
          .attr("font-size", 10)
          .text(function(d) {
            if (d==0){
                return "-17/24";
            }
            if (d==1){
                return "-17/72";
            }
            if (d==2){
                return "17/72";
            }
            if (d==3){
                return "17/24";
            }
          });

        g.selectAll("line-y")
          .data([0,1,2,3])
          .enter().append("line")
          .attr("x1", 0)
          .attr("x2", width)
          .attr("y1", function(d) { return y((d*(2.0/3))+1.5);})
          .attr("y2", function(d) { return y((d*(2.0/3))+1.5);})
          .style("stroke", "grey");

        g.selectAll(".rule-y")
          .data([0,1,2,3])
          .enter().append("text")
          .attr("class", "rule")
          .attr("y", function(d) { return y((d*(2.0/3))+1.5); })
          .attr("x", -13)
          .attr("dy", 3)
          .attr("text-anchor", "middle")
          .attr("font-size", 10)
          .text(function(d) {
            if (d==0){
                return "1.5";
            }
            if (d==1){
                return "13/6";
            }
            if (d==2){
                return "17/6";
            }
            if (d==3){
                return "3.5";
            }
          });

        g.selectAll("scatter-dots")
          .data(data)
          .enter().append("svg:circle")
              .attr("cx", function (d) { console.log(d); return x(d.plate_x); } )
              .attr("cy", function (d) { return y(d.plate_z); } )
              .attr("r", 5)
              .attr("fill", function(d){ return d.color; });

        ReactDOM.render(<Player player={data} />, document.getElementById('player-stats'));
    }
}
document.addEventListener("DOMContentLoaded", function(event) {
  var myLink = document.getElementById('mylink');
  myLink.onclick = function(){
    console.log(myLink);
    make_plots();
  }
});

ReactDOM.render(
	<div>
		<Batters.batters label="Batters" id="batters" />
		<Pitchers.pitchers label="Bullpen" id="pitchers" />
		{/*
		<SelectedValuesField label="Option Creation (tags mode)" options={FLAVOURS} allowCreate hint="Enter a value that's NOT in the list, then hit return" />
		*/}
	</div>,
	document.getElementById('example')
);


