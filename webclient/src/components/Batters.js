import React from 'react';
import Select from 'react-select';

const BATTERS = require('../data/batters');


var value;
var BatterField = React.createClass({
    displayName: 'MultiSelectField',
    propTypes: {
        label: React.PropTypes.string,
    },
    getInitialState () {
        return {
            disabled: false,
            options: BATTERS,
            value: [],
        };
    },
    handleSelectChange (value) {
        console.log('You\'ve selected:', value);
    },
	updateValue (newValue) {
		console.log('Batter changed to ' + newValue);
        value = newValue;
		this.setState({
			selectValue: newValue
		});
	},
    render () {
        var options = BATTERS.Players;
        return (
            <div className="batters-list">
                <h3 className="section-heading">{this.props.label}</h3>
				<Select ref="batterSelect" autofocus options={options} simpleValue clearable={this.state.clearable} name="selected-batter" disabled={this.state.disabled} value={this.state.selectValue} onChange={this.updateValue} searchable={this.state.searchable} />
            </div>
        );
    }
});

module.exports = {
    'batters': BatterField,
    'id': function() {
        return value;
    }
};
