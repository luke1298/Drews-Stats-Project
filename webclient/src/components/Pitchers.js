import React from 'react';
import Select from 'react-select';

const PITCHERS = require('../data/pitchers');;

var value_access;
var PitcherField = React.createClass({
	displayName: 'Pitcher(s)',
	propTypes: {
		label: React.PropTypes.string,
	},
	getInitialState () {
		return {
			options: PITCHERS,
			value: [],
		};
	},
	handleSelectChange (value) {
		console.log('You\'ve selected:', value);
		this.setState({ value });
        value_access = value;
	},
	toggleDisabled (e) {
		this.setState({ disabled: e.target.checked });
	},
	render () {
		return (
			<div className="pitchers-list">
				<h3 className="section-heading">{this.props.label}</h3>
				<Select multi simpleValue disabled={this.state.disabled} value={this.state.value} placeholder="Pick your Pitcher(s)" options={this.state.options} onChange={this.handleSelectChange} />
			</div>
		);
	}
});

module.exports = {
    'pitchers': PitcherField,
    'ids': function() {
        return value_access;
    }
};
