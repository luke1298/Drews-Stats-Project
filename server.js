var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var exec = require('child_process').exec;



// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.route('/exampleR/:batter/:pitchers').get(function(req, res){
    res.setHeader("Access-Control-Allow-Origin", "*");
    console.log("HERE");
    exec('Rscript matchup.R ' + req.params.batter + " " + req.params.pitchers, function(error, stdout, stderr) {
        res.send(stdout);
        if (error !== null) {
            console.log('exec error: '+ error);
        }
    });
  });

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
